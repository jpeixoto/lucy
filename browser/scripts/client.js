$(document).ready(function() {
	ws = new WebSocket("ws://" + location.hostname + ":8080/");

	function stringGen(key)
	{
		var text = "";
		var charset = "abcdefghijklmnopqrstuvwxyz0123456789";

		for( var i=0; i < key; i++ )
			text += charset.charAt(Math.floor(Math.random() * charset.length));

		return text;
	}

	var key = stringGen(5);

	ws.onopen = function() {
		$('#key').text("Key: " + key);
		console.log("key: " + key);
	};

	var gpsCheckBox = 0;
	var lightSensorCheckBox = 0;
	var accCheckBox = 0;
	var boxAggre = 0;
	var boxTrigger = 0;
	var contextHighLevel = 0;

	$( function() {
		var handle = $( "#custom-handle" );
		$( '#slider').slider({
			value: 0.0,
			step: 0.1,
			create: function() {
				handle.text( $( this ).slider( "value" ) );
			},
			slide: function( event, ui ) {
				handle.text( ui.value );
			},
			change: function(event, ui) {
				ws.send(key + "," + "acc, " + ui.value);
			}
		});
		$( "#slider" ).slider( "disable" );
	} );

	$("#balloon1").delay(4000).fadeOut(500);
	$("#balloon2").delay(4500).fadeIn(500);
	$("#balloon2").delay(4000).fadeOut(500);

	function enableBox(fieldId, divId, idNextBox, action) {
		var typeAttribute = $('#'+ divId).data('tipo');
		var selectedBox = $('#' + fieldId + ' .'+typeAttribute);
		var boxStatus = $('#' + fieldId + ' .'+typeAttribute + ' p:eq(1)');
		var nextSelectedBox = $('#' + fieldId).prev().find('.'+typeAttribute);

		if ( action == 'add' ) {
			$(selectedBox).addClass('greenBorder');
			$(boxStatus).addClass('able');
			$(boxStatus).text("Activated");
			$('#' + idNextBox).addClass('available');
			$('#' + idNextBox + ' p:eq(1)').text('Deactivated');
			$('#' + idNextBox + ' p:eq(1)').addClass('available');

		} else if ( action == 'remove') {
			$(selectedBox).removeClass('greenBorder');
			$(boxStatus).removeClass('able');
			$(boxStatus).text("Deactivated");
			$('#' + idNextBox).removeClass('available');
			$('#' + idNextBox + ' p:eq(1)').text('Disabled');
			$('#' + idNextBox + ' p:eq(1)').removeClass('available');
		}
	};

	function enableBoxInt(fieldId, divId, action) {
		var typeAttribute = $('#'+ divId).data('tipo');
		var selectedBox = $('#' + fieldId + ' .'+typeAttribute);
		var boxStatus = $('#' + fieldId + ' .'+typeAttribute + ' p:eq(1)');
		var nextSelectedBox = $('#' + fieldId).prev().find('.'+typeAttribute);

		if ( action == 'add' ) {
			$(selectedBox).addClass('greenBorder');
			$(boxStatus).addClass('able');
			$(boxStatus).text("Activated");
			
		} else if ( action == 'remove') {
			$(selectedBox).removeClass('greenBorder');
			$(boxStatus).removeClass('able');
			$(boxStatus).text("Deactivated");
		}
	};

	$("#gps-enable .status-box").bind("click", function(event) {
		if ($("#gps-enable").data("enabled") === false) {
			$("#gps-enable").data("enabled",true);
			ws.send(key + "," + "9, " + "gps-enable-yes");
			enableBox('fieldset-sensors','gps-enable', 'loc_atual', 'add');
			gpsCheckBox = gpsCheckBox + 1;

		} else if(gpsCheckBox > 1 || $("#loc_atual").data("enabled") === true){
			alert("Disable the superior features first.");

		} else if ($("#loc_atual").data("enabled") === false){
			$("#gps-enable").data("enabled",false);
			ws.send(key + "," + "9, " + "gps-enable-no");
			enableBox('fieldset-sensors', 'gps-enable', 'loc_atual', 'remove');
			gpsCheckBox = gpsCheckBox - 1;
		}
	});

	$('#lightSensor-enable .status-box').bind("click", function(event) {
		if ($("#lightSensor-enable").data("enabled") === false) {
			$("#lightSensor-enable").data("enabled",true);
			ws.send(key + "," + "10, " + "light-enable-yes");
			enableBox('fieldset-sensors','lightSensor-enable', 'lightSensor', 'add');
			lightSensorCheckBox = lightSensorCheckBox + 1;

		} else if(lightSensorCheckBox > 1 || $("#lightSensor").data("enabled") === true){
			alert("Disable the superior features first.");

		}else if ($("#lightSensor").data("enabled") === false){
			$("#lightSensor-enable").data("enabled",false);
			ws.send(key + "," + "10, " + "light-enable-no");
			enableBox('fieldset-sensors', 'lightSensor-enable', 'lightSensor', 'remove');
			lightSensorCheckBox = lightSensorCheckBox - 1;
		}
	});	

	$('#accSensor-enable .status-box').bind("click", function(event) {
		if ($("#accSensor-enable").data("enabled") === false) {
			$("#accSensor-enable").data("enabled",true);
			ws.send(key + "," + "11, " + "acc-enable-yes");
			enableBox('fieldset-sensors','accSensor-enable', 'accSensor', 'add');
			accCheckBox = accCheckBox + 1;

		} else if(accCheckBox > 1 || $("#accSensor").data("enabled") === true){
			alert("Disable the superior features first.");

		}else if ($("#accSensor").data("enabled") === false){
			$("#accSensor-enable").data("enabled",false);
			ws.send(key + "," + "11, " + "acc-enable-no");
			enableBox('fieldset-sensors', 'accSensor-enable', 'accSensor', 'remove');
			accCheckBox = accCheckBox - 1;
		}
	});

	$('#loc_atual .status-box').bind("click", function(event) {
		if ($("#loc_atual").data("enabled") === false && $("#gps-enable").hasClass('greenBorder')) {
			$("#loc_atual").data("enabled",true);
			ws.send(key + "," + "3, " + "gps-yes");
			enableBox('fieldset-widgets','loc_atual', 'new-address', 'add');
			gpsCheckBox = gpsCheckBox + 1;

		} else if(gpsCheckBox > 2 || $("#new-address").data("enabled") === true){
			alert("Disable the superior features first.");

		} else if($("#new-address").data("enabled") === false && $("#loc_atual").hasClass('available')){
			$("#loc_atual").data("enabled",false);
			ws.send(key + "," + "3, " + "gps-no");
			enableBox('fieldset-widgets','loc_atual', 'new-address', 'remove');
			gpsCheckBox = gpsCheckBox - 1;
		}
	});

	$('#lightSensor .status-box').bind("click", function(event) {
		if ($("#lightSensor").data("enabled") === false && $("#lightSensor-enable").hasClass('greenBorder')) {
			$("#lightSensor").data("enabled",true);
			ws.send(key + "," + "4, " + "light-yes");
			enableBox('fieldset-widgets','lightSensor', 'lightInter', 'add');
			$('#flashlightAggre2').addClass('available');
			$('#flashlightAggre2 .status-box').text('Deactivated');
			$('#flashlightAggre2 .status-box').addClass('available');
			lightSensorCheckBox = lightSensorCheckBox + 1;

		} else if(lightSensorCheckBox > 2 || $("#lightInter").data("enabled") === true){
			alert("Disable the superior features first.");

		} else if($("#lightInter").data("enabled") === false && $("#lightSensor").hasClass('available')){
			$("#lightSensor").data("enabled",false);
			ws.send(key + "," + "4, " + "light-no");
			enableBox('fieldset-widgets','lightSensor', 'lightInter', 'remove');
			$('#flashlightAggre2').removeClass('available');
			$('#flashlightAggre2 .status-box').text('Disabled');
			$('#flashlightAggre2 .status-box').removeClass('available');
			lightSensorCheckBox = lightSensorCheckBox - 1;
		}
	});

	$('#accSensor .status-box').bind("click", function(event) {
		if ($("#accSensor").data("enabled") === false && $("#accSensor-enable").hasClass('greenBorder')) {
			$("#accSensor").data("enabled",true);
			ws.send(key + "," + "5, " + "acc-yes");
			enableBox('fieldset-widgets','accSensor', 'accInter', 'add');
			accCheckBox = accCheckBox + 1;

		} else if(accCheckBox > 2 || $("#accInter").data("enabled") === true){
			alert("Disable the superior features first.");

		} else if($("#accInter").data("enabled") === false && $("#accSensor").hasClass('available')){
			$("#accSensor").data("enabled",false);
			ws.send(key + "," + "5, " + "acc-no");
			enableBox('fieldset-widgets','accSensor', 'accInter', 'remove');
			accCheckBox = accCheckBox - 1;
		}
	});

	$('#new-address .status-box').bind("click", function(event) {
		if ($("#new-address").data("enabled") === false && $("#loc_atual").hasClass('greenBorder')) {
			$("#new-address").data("enabled",true);
			ws.send(key + "," + "20, " + "map-yes");
			
			if($('#address-text').val()==="Localização Definida"){
				ws.send(key + "," + "1, " + $('#address-text').val());
 				ws.send(key + "," + "Lat: " + $("#lat").val() + "," + " Long: "+ $("#lgt").val());
			} 
			else if($('#address-text').val()!= "Localização Definida"){
 				ws.send(key + "," + "1, " + $('#address-text').val());
			}

			$("#address-text").removeAttr('disabled');
			gpsCheckBox = gpsCheckBox + 1;
			boxAggre = boxAggre + 1;
			enableBoxInt('fieldset-interpreters','new-address', 'add');

			if(boxAggre > 1){
				$('#characAggre').addClass('available');
				$('#characAggre .status-box').text('Deactivated');
				$('#characAggre .status-box').addClass('available');
			}

		} else if(gpsCheckBox > 3 || $("#characAggre").data("enabled") === true){
			alert("Disable information related to this functionality first.");
		}
		else if ($("#new-address").data("enabled") === true){
			$("#new-address").data("enabled",false);
			ws.send(key + "," + "20, " + "map-no");
			$("#address-text").attr("disabled", true);
			gpsCheckBox = gpsCheckBox - 1;
			boxAggre = boxAggre - 1;
			enableBoxInt('fieldset-interpreters','new-address', 'remove');

			if(boxAggre <= 1){
				$('#characAggre').removeClass('available');
				$('#characAggre .status-box').text('Disabled');
				$('#characAggre .status-box').removeClass('available');
			}
		}

	});

	$('#address-text').change(function() {
		ws.send(key + "," + "1, " + $(this).val());

		var x = document.getElementById("address-text").value;
		if (x.localeCompare("Localização Definida")==0){
			openbox('Enter the coordinates in decimal degrees (without º)', 0);
		}
	});

	$("#gps_form").bind("submit", function(event) {
		ws.send(key + "," + "Lat: " + $("#lat").val() + "," + " Long: "+ $("#lgt").val());
		document.getElementById('box').style.display='none';
		document.getElementById('shadowing').style.display='none';

		if($("#loc-text option[value='Localização Definida']").length ==0){
			var x = document.getElementById("loc-text");
			var c = document.createElement("option");
			c.text = "Definite location";
			c.value = "Localização Definida";
			x.options.add(c, 2);
		}
	});

	$('#lightInter .status-box').bind("click", function(event) {
		if ($("#lightInter").data("enabled") === false && $("#lightSensor").hasClass('greenBorder')) {
			$("#lightInter").data("enabled",true);
			ws.send(key + "," + "6, " + "lightInter-yes");
			$("#lightInter input").removeAttr('disabled');
			lightSensorCheckBox = lightSensorCheckBox + 1;
			boxAggre = boxAggre + 1;
			boxTrigger = boxTrigger + 1;
			enableBoxInt('fieldset-interpreters','lightInter', 'add');

			if(boxAggre > 1){
				$('#characAggre').addClass('available');
				$('#characAggre .status-box').text('Deactivated');
				$('#characAggre .status-box').addClass('available');
			}

			if(boxTrigger > 1) {
				$('#flashlightAggre').addClass('available');
				$('#flashlightAggre .status-box').text('Deactivated');
				$('#flashlightAggre .status-box').addClass('available');
			}

		} else if(lightSensorCheckBox > 4 || $("#characAggre").data("enabled") === true
			|| $("#flashlightAggre").data("enabled") === true){
			alert("Disable information related to this functionality first.");
		}
		else if ($("#lightInter").data("enabled") === true){
			$("#lightInter").data("enabled",false);
			ws.send(key + "," + "6, " + "lightInter-no");
			$("#lightInter input").attr("disabled", true);
			lightSensorCheckBox = lightSensorCheckBox - 1;
			boxAggre = boxAggre - 1;
			boxTrigger = boxTrigger - 1;
			enableBoxInt('fieldset-interpreters','lightInter', 'remove');

			if(boxAggre <= 1){
				$('#characAggre').removeClass('available');
				$('#characAggre .status-box').text('Disabled');
				$('#characAggre .status-box').removeClass('available');
			} 

			if(boxTrigger <= 1){
				$('#flashlightAggre').removeClass('available');
				$('#flashlightAggre .status-box').text('Disabled');
				$('#flashlightAggre .status-box').removeClass('available');
			}
		}
	});

	$("#low-light-sensor").bind("submit", function(event) {
		ws.send(key + "," + "nolight, " + $("#low-light-sensor-text").val());
	});

	$("#high-light-sensor").bind("submit", function(event) {
		ws.send(key + "," + "bright, " + $("#high-light-sensor-text").val());
	});

	$('#accInter .status-box').bind("click", function(event) {
		if ($("#accInter").data("enabled") === false && $("#accSensor").hasClass('greenBorder')) {
			$("#accInter").data("enabled",true);
			ws.send(key + "," + "7, " + "accInter-yes");
			$( "#slider" ).slider( "enable" );
			
			accCheckBox = accCheckBox + 1;
			boxTrigger = boxTrigger + 1;
			enableBoxInt('fieldset-interpreters','accInter', 'add');

			if(boxTrigger > 1) {
				$('#flashlightAggre').addClass('available');
				$('#flashlightAggre .status-box').text('Deactivated');
				$('#flashlightAggre .status-box').addClass('available');
			}

		}else if(accCheckBox > 3 || $("#flashlightAggre").data("enabled") === true){
			alert("Disable information related to this functionality first.");
		}
		else if ($("#accInter").data("enabled") === true){
			$("#accInter").data("enabled",false);
			$( "#slider" ).slider( "disable" );
			ws.send(key + "," + "7, " + "accInter-no");
			accCheckBox = accCheckBox - 1;
			boxTrigger = boxTrigger - 1;
			enableBoxInt('fieldset-interpreters','accInter', 'remove');

			if(boxTrigger <= 1){
				$('#flashlightAggre').removeClass('available');
				$('#flashlightAggre .status-box').text('Disabled');
				$('#flashlightAggre .status-box').removeClass('available');
			}
		}
	});

	$('#context-text').change(function() {
		ws.send(key + "," + "12, " + $(this).val());
		contextHighLevel  = contextHighLevel + 1;
		if(contextHighLevel > 3){
			$("#flashlightAggre3").addClass('available');
			$('#flashlightAggre3 .status-box').text('Deactivated');
			$('#flashlightAggre3 .status-box').addClass('available');
		}
	});

	$('#loc-text').change(function() {
		ws.send(key + "," + "13, " + $(this).val());
		contextHighLevel  = contextHighLevel + 1;
		if(contextHighLevel > 3){
			$("#flashlightAggre3").addClass('available');
			$('#flashlightAggre3 .status-box').text('Deactivated');
			$('#flashlightAggre3 .status-box').addClass('available');
		}
	});

	$('#lum-options').change(function() {
		ws.send(key + "," + "14, " + $(this).val());
		contextHighLevel  = contextHighLevel + 1;
		if(contextHighLevel > 3){
			$("#flashlightAggre3").addClass('available');
			$('#flashlightAggre3 .status-box').text('Deactivated');
			$('#flashlightAggre3 .status-box').addClass('available');
		}
	});
	
    $( function() {
		var handle = $( "#custom-handle-lum" );
		$( '#lum-options3').slider({
			value: 0,
			step: 5,
			max: 1000,
			create: function() {
				handle.text( $( this ).slider( "value" ) );
			},
			slide: function( event, ui ) {
				handle.text( ui.value );
			},
			change: function(event, ui) {
				ws.send(key + "," + "15," + ui.value);
			}
		});
		$( "#lum-options3" ).slider( "disable" );
	} );

	$('#enable-flashlight2').change(function() {
		ws.send(key + "," + "16, " + $(this).val());
		$('#flashlightAggre2').addClass('greenBorder');
	});

	$('#context-text2').change(function() {
		ws.send(key + "," + "17, " + $(this).val());
		$('#flashlightAggre3').addClass('greenBorder');
	});

	$('#enable-flashlight3').change(function() {
		ws.send(key + "," + "18, " + $(this).val());
		$('#flashlightAggre3').addClass('greenBorder');
	});

	$('#lum-options2').change(function() {
		ws.send(key + "," + "19, " + $(this).val());
		$('#flashlightAggre').addClass('greenBorder');
	});

	$('#acc-display').change(function() {
		ws.send(key + "," + "21, " + $(this).val());
		$('#flashlightAggre').addClass('greenBorder');
	});

	$('#enable-flashlight').change(function() {
		ws.send(key + "," + "8, " + $(this).val());
		$('#flashlightAggre').addClass('greenBorder');
	});	

	//aggregator
	$('#characAggre .status-box').bind("click", function(event) {
		if ($("#characAggre").data("enabled") === false && boxAggre > 1) {
			$("#characAggre").data("enabled",true);
			$("#context-text").removeAttr('disabled');
			$("#loc-text").removeAttr('disabled');
			$("#lum-options").removeAttr('disabled');

			ws.send(key + "," + "12, " + $("#context-text").val());
			ws.send(key + "," + "13, " + $("#loc-text").val());
			ws.send(key + "," + "14, " + $("#lum-options").val());

			lightSensorCheckBox = lightSensorCheckBox + 1;
			gpsCheckBox = gpsCheckBox + 1;
			contextHighLevel = contextHighLevel + 1;

			$("#characAggre").addClass('greenBorder');
			$('#characAggre .status-box').addClass('able');
			$('#characAggre .status-box').text("Activated");

			if(contextHighLevel > 3){
				$("#flashlightAggre3").addClass('available');
				$('#flashlightAggre3 .status-box').text('Deactivated');
				$('#flashlightAggre3 .status-box').addClass('available');
			}

		} else if(boxAggre > 2){
			alert("Disable the corresponding trigger first.");
		} 
		else if ($("#characAggre").data("enabled") === true){
			$("#characAggre").data("enabled",false);
			ws.send(key + "," + "12, " + "");
			ws.send(key + "," + "13, " + "");
			ws.send(key + "," + "14, " + "");

			$("#context-text").attr("disabled", true);
			$("#loc-text").attr("disabled", true);
			$("#lum-options").attr("disabled", true);

			lightSensorCheckBox = lightSensorCheckBox - 1;
			gpsCheckBox = gpsCheckBox - 1;
			contextHighLevel = contextHighLevel - 1;

			$("#characAggre").removeClass('greenBorder');
			$('#characAggre .status-box').removeClass('able');
			$('#characAggre .status-box').text("Deactivated");

			$("#flashlightAggre3").removeClass('available');
			$('#flashlightAggre3 .status-box').text('Disabled');
			$('#flashlightAggre3 .status-box').removeClass('available');
		}

	});

	//context trigger
	$('#flashlightAggre3 .status-box').bind("click", function(event) {
		if ($("#flashlightAggre3").data("enabled") === false && contextHighLevel > 3 && $("#flashlightAggre3").hasClass("available")) {
			$("#flashlightAggre3").data("enabled",true);
			$('#enable-flashlight3').removeAttr('disabled');
			$("#context-text2").removeAttr('disabled');

			ws.send(key + "," + "17, " + $('#context-text2').val());
			ws.send(key + "," + "18, " + $('#enable-flashlight3').val());

			lightSensorCheckBox = lightSensorCheckBox + 1;
			boxAggre = boxAggre + 1;

			$("#flashlightAggre3").addClass('greenBorder');
			$('#flashlightAggre3 .status-box').addClass('able');
			$('#flashlightAggre3 .status-box').text("Activated");

		}else if($("#flashlightAggre3").data("enabled") === false && contextHighLevel > 2){
			alert("Enable the aggregator.");
		} 
		else if($("#flashlightAggre3").data("enabled") === true){
			$("#flashlightAggre3").data("enabled",false);
			
			ws.send(key + "," + "17, " + "");
			ws.send(key + "," + "18, " + "");

			$('#enable-flashlight3').attr("disabled", true);
			$("#context-text2").attr("disabled", true);

			lightSensorCheckBox = lightSensorCheckBox - 1;
			boxAggre = boxAggre - 1;

			$("#flashlightAggre3").removeClass('greenBorder');
			$('#flashlightAggre3 .status-box').removeClass('able');
			$('#flashlightAggre3 .status-box').text("Deactivated");
		}

	});

	//light trigger
	$('#flashlightAggre2 .status-box').bind("click", function(event) {
		if ($("#flashlightAggre2").data("enabled") === false && $("#lightSensor").data("enabled") === true) {
			$("#flashlightAggre2").data("enabled",true);
			$("#lum-options3").removeAttr('disabled');
			$("#lum-options3").slider( "enable" );
			$("#enable-flashlight2").removeAttr('disabled');

			ws.send(key + "," + "15," + $("#lum-options3").slider( "value" ));
			ws.send(key + "," + "16, " + $('#enable-flashlight2').val());

			lightSensorCheckBox = lightSensorCheckBox + 1;

			$("#flashlightAggre2").addClass('greenBorder');
			$('#flashlightAggre2 .status-box').addClass('able');
			$('#flashlightAggre2 .status-box').text("Activated");

		}else if($("#flashlightAggre2").data("enabled") === true){
			$("#flashlightAggre2").data("enabled",false);
			
			ws.send(key + "," + "16, " + "");

			$("#lum-options3").attr("disabled", true);
			$("#lum-options3").slider( "disable" );
			$("#enable-flashlight2").attr("disabled", true);

			lightSensorCheckBox = lightSensorCheckBox - 1;

			$("#flashlightAggre2").removeClass('greenBorder');
			$('#flashlightAggre2 .status-box').removeClass('able');
			$('#flashlightAggre2 .status-box').text("Deactivated");
		}

	});

	//compound trigger
	$('#flashlightAggre .status-box').bind("click", function(event) {
		if ($("#flashlightAggre").data("enabled") === false && boxTrigger > 1) {
			$("#flashlightAggre").data("enabled",true);
			$("#lum-options2").removeAttr('disabled');
			$("#acc-display").removeAttr('disabled');
			$("#enable-flashlight").removeAttr('disabled');

			ws.send(key + "," + "19, " + $("#lum-options2").val());
			ws.send(key + "," + "21, " + $('#acc-display').val());
			ws.send(key + "," + "8, " + $('#enable-flashlight').val());

			lightSensorCheckBox = lightSensorCheckBox + 1;
			accCheckBox = accCheckBox + 1;
			boxTrigger = boxTrigger + 1;

			$("#flashlightAggre").addClass('greenBorder');
			$('#flashlightAggre .status-box').addClass('able');
			$('#flashlightAggre .status-box').text("Activated");

		}else if($("#flashlightAggre").data("enabled") === true){
			$("#flashlightAggre").data("enabled",false);
			
			ws.send(key + "," + "19, " + "");
			ws.send(key + "," + "21, " + "");
			ws.send(key + "," + "8, " + "");

			$("#lum-options2").attr("disabled", true);
			$("#acc-display").attr("disabled", true);
			$("#enable-flashlight").attr("disabled", true);

			lightSensorCheckBox = lightSensorCheckBox - 1;
			accCheckBox = accCheckBox - 1;
			boxTrigger = boxTrigger - 1;

			$("#flashlightAggre").removeClass('greenBorder');
			$('#flashlightAggre .status-box').removeClass('able');
			$('#flashlightAggre .status-box').text("Deactivated");
		}

	});

	$("#sync-section input").click(function() {
		$("#simulation").css('display', 'inline-block');
		$("#sync-section").hide();
	});

	$("#tuto-start").click(function() {
		$("#sync-section").css('display', 'inline-block');
		$("#tuto-section").hide();
	});

	$( "#menu input" ).click(function() {
			$("#menu input").removeClass('bt-active');
			$("#bt-triggers").css("background", "url(/image/triggers_0.png)");
			$("#bt-triggers").css("background-repeat", "no-repeat");
			$("#bt-triggers").css("background-size", "197px");
			$("#bt-aggregator").css("background", "url(/image/agg_0.png)");
			$("#bt-aggregator").css("background-repeat", "no-repeat");
			$("#bt-aggregator").css("background-size", "140px"); 
			if($(this).hasClass("bt-image-trig")){
				$(this).css("background", "url(/image/triggers_1.png)");
				$(this).css("background-repeat", "no-repeat"); 
				$(this).css("background-size", "197px");
			}else if($(this).hasClass("bt-image-agg")){
				$(this).css("background", "url(/image/agg_1.png)");
				$(this).css("background-repeat", "no-repeat"); 
				$(this).css("background-size", "140px"); 
			}else if(!$(this).hasClass("bt-image-agg") || !$(this).hasClass("bt-image-trig")){
				$(this).addClass("bt-active");
			}
	});

});

// Change layer in simulation
function changeLayer(layer1, layer2, layer3, layer4, layer5)
{
	document.getElementById(layer1).style.display='inline-block';
	document.getElementById(layer2).style.display='none';
	document.getElementById(layer3).style.display='none';
	document.getElementById(layer4).style.display='none';
	document.getElementById(layer5).style.display='none';
}

//lightbox coordinates
function gradient(id, level)
{
	var box = document.getElementById(id);
	box.style.opacity = level;
	box.style.MozOpacity = level;
	box.style.KhtmlOpacity = level;
	box.style.filter = "alpha(opacity=" + level * 100 + ")";
	box.style.display="block";
	return;
}

function fadein(id) 
{
	var level = 0;
	while(level <= 1)
	{
		setTimeout( "gradient('" + id + "'," + level + ")", (level* 1000) + 10);
		level += 0.01;
	}
}

// Open the lightbox
function openbox(formtitle, fadin)
{
	var box = document.getElementById('box'); 
	document.getElementById('shadowing').style.display='block';

	var btitle = document.getElementById('boxtitle');
	btitle.innerHTML = formtitle;

	if(fadin)
	{
		gradient("box", 0);
		fadein("box");
	}
	else
	{ 	
		box.style.display='block';
	}
}

// Close the lightbox
function closebox()
{
	document.getElementById('box').style.display='none';
	document.getElementById('shadowing').style.display='none';
}

//Tooltip
$( function() {
	$( document ).tooltip({
		position: {
			my: "center bottom+85",
			at: "center",
			using: function( position, feedback ) {
				$( this ).css( position );
				$( "<div>" )
				.addClass( "arrow" )
				.addClass( feedback.vertical )
				.addClass( feedback.horizontal )
				.appendTo( this );
			}
		}
	});
} );