Ambiente para ensino de conceitos de Computação Ubíqua para estudantes da graduação e pós-graduação. // Environment for teaching concepts of Ubiquitous Computing for undergraduate and graduate students.

[Presentation of the Lucy environment](https://1drv.ms/v/s!AnfTpOzL2MeOiOpnBbtiU5Gbr_bI_Q)

![Presentation of the lucy](docs/lucy.png)

![](docs/sync_tela.png)

![](docs/tela_mobile.png)

# Instruções de configuração do Lucy

## Acessar o servidor no great
- É necessário ter o arquivo **.pem**, que é a chave de acesso ao servidor
- Execute o seguinte comando no mesmo diretório onde o arquivo **.pem** estiver: `ssh -i joelmapereira.pem -p 61001 ubuntu@200.19.179.222`

## Instalação de dependências
- Lucy depende de: git, node.js, mongodb, ruby, bundler, supervisor e nginx;
- Adicionar o repositório do nodejs: ` curl -sL https://deb.nodesource.com/setup_6.x | sudo -E bash -`
- Instalar as dependências: `sudo apt-get install -y git nodejs mongodb ruby-full supervisor nginx`

### Obter o repositório do projeto Lucy do bitbucket
- Clonar o repositório: `git clone https://jpeixoto@bitbucket.org/jpeixoto/lucy.git`
- Criar diretório para armazenar logs: `mkdir lucy/logs`

## Execução dos módulos
- A inicilização dos módulos será gerenciada pelo serviço **supervisor**, portanto não é necessário iniciá-los manualmente

### Server
- Acesse o diretório do módulo **server**: `cd lucy/server`
- Instale as dependências do módulo: `bundle install`
- Para iniciar o módulo manualmente: `ruby server.rb`

### Node-login
- Acesse o diretório do módulo **node-login**: `cd lucy/node-login`
- Instale as dependências do módulo: `npm install`
- Para iniciar o módulo manualmente: `node app`

### Browser
- Acesse o diretório do módulo **browser**: `cd lucy/browser`
- É possível iniciar o módulo manualmente de duas maneiras:
    - Python: `python -m SimpleHTTPServer`
    - Ruby: `ruby -run -ehttpd . -p8000`

### Supervisor
- Copiar o arquivo de configuração: `sudo cp lucy/conf/lucy.conf /etc/supervisor/conf.d/`
- Iniciar o serviço **supervisor**: `sudo service supervisor restart`
- Para verificar o status dos módulos: `sudo supervisorctl status`
- Caso precise reiniciar os módulos: `sudo supervisorctl restart all`
- Em caso de dúvidas, verificar a documentação do **supervisor**: http://supervisord.org/introduction.html

### Nginx
- Copiar o arquivo de configuração: `sudo cp lucy/conf/lucy-proxy /etc/nginx/sites-available/`
- Remover o link padrão do nginx: `sudo rm /etc/nginx/sites-enabled/default`
- Criar um link apontando para o arquivo do lucy: `sudo ln -s /etc/nginx/sites-available/lucy-proxy /etc/nginx/sites-enabled/lucy-proxy`
- Reiniciar nginx: `sudo service nginx restart`
